desc "Build the entire site, including all sub-sites"
task :build_all do
  build_cmd = 'middleman build --bail'

  # Build top level
  puts "Building top level..."
  system(build_cmd) || raise("command failed: #{build_cmd}")

  # Build sub-sites
  sites = %w[
    handbook
  ]
  sites.each do |site|
    site_dir = "sites/#{site}"
    Dir.chdir(site_dir) do
      puts "\n\nBuilding '#{site}' site from #{site_dir}..."
      system(build_cmd) || raise("command failed for '#{site}' site in #{site_dir}: #{build_cmd}")
    end
  end
end
