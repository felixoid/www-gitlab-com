module Gitlab
  module CodeOwners
    FILE_NAME = 'CODEOWNERS'.freeze
    FILE_PATHS = [FILE_NAME, "docs/#{FILE_NAME}", ".gitlab/#{FILE_NAME}"].freeze

    autoload :File, 'lib/code_owners/file'

    def self.load_codeowners_file
      file_path = FILE_PATHS.find { |file_path| ::File.exist?(file_path) }
      raise 'CODEOWNERS file is missing' unless file_path

      CodeOwners::File.new(::File.read(file_path))
    end
  end
end
