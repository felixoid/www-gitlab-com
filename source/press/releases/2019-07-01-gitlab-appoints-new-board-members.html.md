---
layout: markdown_page
title: "GitLab Appoints Sue Bostrom and David Hornik to Board of Directors"
---

_Board additions further GitLab’s position as the first single application for the entire DevOps lifecycle and support continued company growth_

**SAN FRANCISCO, Calif. – July 1, 2019 –** [GitLab](https://about.gitlab.com/), the single application for the DevOps lifecycle, announced today the appointment of Sue Bostrom, formerly the executive vice president and Chief Marketing Officer at Cisco Systems, and David Hornik, General Partner at August Capital, to its board of directors. With their combined experience, they are uniquely positioned to guide GitLab through its next phase of growth. 

“As we continue to expand as a company, it is important we bring together great leaders in the industry to contribute to our success. Having the insight of Sue Bostrom and David Hornik will help take us to the next level as an organization,” said Sid Sijbrandij, co-founder and CEO of GitLab. “Both have long-standing experience working with innovative startups and established industry leaders, which will be instrumental to our long-term growth strategy.”

Bostrom was at Cisco Systems Inc. for almost 14 years, spending the last five years of her tenure as Executive Vice President, Chief Marketing Officer (CMO), and head of Worldwide Government Affairs. She was responsible for developing Cisco's vision and strategy through positioning, branding, advertising, and product marketing. Prior to serving as CMO, Bostrom built and led Cisco's Internet Business Solutions Group, bringing technology best practices to Global Fortune 500 companies and heads of state. In addition to her work at Cisco, Bostrom has over 15 years of experience serving on boards of successful technology companies including ServiceNow, Anaplan, Nutanix, Cadence Design Systems and Marketo.  

“GitLab is changing the way organizations work by unifying teams through its single application for the entire DevOps lifecycle offering,” said Sue Bostrom. “Having served on the boards of many high growth companies, I believe GitLab’s 100% remote workforce and transparent culture will be a key differentiator for the company in scaling to meet market demand. I look forward to contributing to GitLab’s future success.”

Hornik invests broadly in information technology companies, with a focus on enterprise applications and infrastructure software. Having just entered his 20th year at venture capital firm August Capital, Hornik will provide guidance on GitLab’s strategy and vision as it moves towards its goal of continued growth. Hornik serves on several boards including Fastly (FSLY), Bill.com, Insight Engines, and Second Spectrum, and sat on the board of Splunk for 13 years, from inception through IPO. Hornik teaches business and law at Harvard Law School and Stanford’s Graduate School of Business. August Capital led GitLab’s Series B round in 2016.

“When I was first introduced to GitLab’s CEO Sid Sijbrandij at Y Combinator, I knew they were doing something special. Sid had a vision to build a scalable and integrated platform that addresses a major pain point for developers and he’s since been able to bring that vision to fruition,” said David Hornik. “Having lived and breathed technology for the last 20 years, I hope to bring to GitLab a broad perspective to help the company execute on its next phase of growth.”

Bostrom and Hornik join current standing board members Sid Sijbrandij, CEO of GitLab, Larry Augustin, Managing Director of Augustin Ventures,  Bruce Armstrong, Operating Partner of Khosla Ventures and Matthew Jacobson, General Partner of Iconiq Strategic Partners. For more information on GitLab’s Board of Directors please see [here](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/726). 

#### About GitLab
GitLab is a DevOps platform built from the ground up as a single application for all stages of the DevOps lifecycle enabling Product, Development, QA, Security, and Operations teams to work concurrently on the same project. GitLab provides a single data store, one user interface, and one permission model across the DevOps lifecycle. This allows teams to significantly reduce cycle time through more efficient collaboration and enhanced focus. Built on Open Source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations from startups to global enterprises, including Ticketmaster, Jaguar Land Rover, NASDAQ, Dish Network, and Comcast trust GitLab to deliver great software faster. GitLab is the world's largest all-remote company, with more than 1,200 team members in more than 65 countries and regions.
#### Media Contact
Natasha Woods
<br>
GitLab
<br>
(415) 312-5289
