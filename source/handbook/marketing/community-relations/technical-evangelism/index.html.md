---
layout: handbook-page-toc
title: "Technical Evangelism"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Goal
To build GitLab’s technical brand with deep, meaningful conversations on engineering topics relevant to our industry by leveraging our community of team-members and the wider ecosystem.

### What do we do?

The GitLab technical evangelists are engineers who enjoy learning and teaching the latest technology topics with the wider community. The evangelists are supported by program management within the team and also collaborate closely with the content and PR teams in corporate marketing to amplify their voices. There are two specific goals for the team:

1. **Thought leadership:** Drive GitLab awareness and brand value as full-time technical evangelists participating in thought leadership on au courant topics in the cloud native and cloud computing ecosystem. 

2. **Ecosystem engagement:** Establish GitLab’s technical thought leadership by building GitLab’s influence in the tech community through participation in open source projects, foundations, and other consortiums. A description of consortiums can be found in our [consortiums handbook page](https://about.gitlab.com/handbook/marketing/technical-evangelism/consortiums/)  

3. **Marketing value:** Work with other teams in GitLab (such as content, social, marketing programs) to repurpose, maximize value of, and measure the impact of content created for thought leadership and ecosystem engagement.


## Who we are

We are members of the [Community Relations team](/handbook/marketing/community-relations/).

### Team members and focus areas

1. [Abubakar Siddiq Ango](https://about.gitlab.com/company/team/#abuango) - Technical Evangelism Program Manager
    - Program management
        - Running the CFP process for conferences on the [Technical Evagelism events list](https://docs.google.com/spreadsheets/u/1/d/1E4J4Kx7Eq8JXuh_o4RfSRbGyjj8IeVCiPRLOGslLcfU/edit?usp=drive_web&ouid=101407496565734994973)
        - Organizing Technical evangelism team's content creation and repurposing efforts
    - Kubernetes
    - Cloud Native
    - Language skills: English, Yoruba, Hausa
2. [Brendan O'Leary](https://about.gitlab.com/company/team/#brendan) -  Senior Developer Evangelist
    - DevOps with a focus on the application developer perspective
        - SCM
        - GitOps
        - CI
        - .NET and Javascript communities
    - Language skills: English
3. [Michael Friedrich](https://about.gitlab.com/company/team/#dnsmichi) - Developer Evangelist
    - DevOps with a focus on the SRE, Ops engineers' perspective
        - CI/CD
        - Serverless
        - Observability
    - Language skills: English, German, Austrian

## How we work

### Team Bookmarks
* CFP
    * [Create CFP Meta Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFP-Meta)
    * [Create CFP Submission Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFPsubmission)
* [Create TE Request Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=technical-evangelist-request)
* [Events Spreasheet](https://docs.google.com/spreadsheets/d/1E4J4Kx7Eq8JXuh_o4RfSRbGyjj8IeVCiPRLOGslLcfU/edit#gid=0)
* [Events Calendar](#our-events-list)
* [Team Shared Drive](https://drive.google.com/drive/u/0/folders/0AEUOlCStMBC9Uk9PVA) (Internal)
* [Weekly Meeting Agenda](https://docs.google.com/document/d/1oPXtlWNDbeut-dbFDLXBCfiBJLWwbzjjK9CFu5o8QzU/edit#) (Internal)
* [Team Collab Session Agenda Doc](https://docs.google.com/document/d/1Hs2DrUf9QJQR8fSsNO9WgX7c9sS46NSjEDuKHrrvPv0/edit#heading=h.vgcz1npa9iy) (Internal)
* Team Issue Boards
    * [General](https://gitlab.com/groups/gitlab-com/-/boards/1565342?&label_name[]=tech-evangelism)
    * [CFP](https://gitlab.com/groups/gitlab-com/-/boards/1616902?&label_name[]=TE-CFP)
    * [Content](https://gitlab.com/groups/gitlab-com/-/boards/1624080?&label_name[]=tech-evangelism)
    * [Milestones](https://gitlab.com/groups/gitlab-com/-/boards/1672643?label_name[]=tech-evangelism)


### Issue Tracker
We work in the open using the GitLab's [Corporate Marketing issue tracker](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues) but we own the label `tech-evangelism` which can be applied to issues in any project under the [gitlab-com](https://gitlab.com/gitlab-com) and [gitlab-org](https://gitlab.com/gitlab-org) parent groups. 

You can follow the latest our team is working on by looking at [our label-based issue board](https://gitlab.com/groups/gitlab-com/-/boards/1565342?&label_name[]=tech-evangelism).

### Issue Labels
Using the `tech-evangelism` label on an issue means we are working on it or participating in the ongoing conversation. `tech-evangelism` label is accompanied by a number of labels we use to organize our work.

NB: You only need to use the `tech-evangelism` label on issues requiring the attention of the Technical Evangelism team. Other labels are applied by the team as their state change or as contained in various issue templates.

#### General Labels

| **CFP Labels** |  **Description** |
| ------ | ------ |
| `TE-DueSoon` |  This is used to monitor TE issues that are due soon | 
| `TE-Peer-Review` |  Feedback is needed on the issue from TE team members |
| `TE-Ops` | Used to label issues related to the technical evangelism `Ops in DevOps` theme |
| `TE-Dev` | Used to label issues related to the technical evangelism `Dev in DevOps` theme |
| `TE-k8s` | Used to label issues related to the technical evangelism `Kubernetes` theme |


#### Technical Evangelism Team Administrative Processes
The team creates issues for iteration, team discussions and other issues for internal processes. These issues are tracked using the following labels:

| **Process Labels** | **Description** |
| ------ |  ------ |
| `TE-Process::Open` | Process related issues that are still being discussed or worked on |
| `TE-Process::Pending` | Process related issues that are on hold due to an external factor |
| `TE-Process::Done` | Completed Process issues |
| `TE-Process::FYI` | Issues that require no action from the team, but need to be aware of |

### Request a Technical Evangelist
If you require a Technical Evangelist's help for anything, please use this ["Technical Evangelist Request" issue template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=technical-evangelist-request) to create an issue and we will get back to as soon as possible. On creating the request issue, 2 labels are applied: `TE-Request::New` & `TE-Request-Type::External` and transitioned through different states as listed below:

| **Request Labels** | **Description** |
| ------ |  ------ |
| `TE-Request::New` | Newly created requests |
| `TE-Request::Open` | Requests that are still being worked on or considered |
| `TE-Request::Done` | Completed requests |
| `TE-Request::Cancelled` | Requests not considered by the team |
| `TE-Request-Type::External` | Identifies Requests created by other teams |
| `TE-Request-Type::internal` | Identifies Requests created by Technical Evangelism team members |



#### CFPs (Call for Proposals)

We monitor 2 GitLab issue types for CFPs:

1. A meta issue for call for proposals
1. Submission issues created by interested speakers that relate to the meta issue

All CFP issues need to be labeled with `TE-CFP` first. This allows these issues to appear on the dedicated [CFP Issue board](https://gitlab.com/groups/gitlab-com/-/boards/1616902?&label_name[]=TE-CFP).

Afterwards our workflow uses scoped labels to transition the issues through different stages.

| **CFP Labels** | **Issue Type** | **Description** |
| ------ | ------ | ------ |
| `TE-CFP-Meta::Open` | Meta CFP Issue | Identifies a CFP meta issue that is still open for submissions |
| `TE-CFP-Meta::Closed` | Meta CFP Issue | Identifies a CFP meta issue that already has passed due date |
| `TE-CFP-Meta::Cancelled` | Meta CFP Issue | Identifies a CFP meta issue for an event that has been cancelled |
| `TE-CFP-Upcoming` | Meta CFP Issue  |  This labels is used to track CFPs that are near due dates. | 
| `TE-CFP::Draft` | CFP Submission Issue | Identifies CFP (Call for Proposal) Drafts that are under Technical Evangelism Team Radar | 
| `TE-CFP::Review` | CFP Submission Issue | Identifies CFP (Call for Proposal) Drafts that requires review by the Technical Evangelism Team | 
| `TE-CFP::Submitted` | CFP Submission Issue  | Identifies submitted CFPs (Call for Proposal) and monitored by the Technical Evangelism team |
| `TE-CFP::Rejected` |  CFP Submission Issue  | Identifies Rejected CFPs (Call for Proposal) and monitored by the Technical Evangelism team |
| `TE-CFP::Accepted` | CFP Submission Issue  | Identifies Accepted CFPs (Call for Proposal) and monitored by the Technical Evangelism team | 
| `TE-CFP::Missed` | CFP Submission Issue  | Identifies CFPs (Call for Proposal) that missed submission | 


For a CFP submission the transition is depicted below:

```plantuml
start
: TE-CFP, TE-CFP::Draft;
: TE-CFP, TE-CFP::Review;
: TE-CFP, TE-CFP::Submitted;
if (CFP is Accepted) then (yes)
    : TE-CFP, TE-CFP::Accepted;
elseif (CFP is Rejected) then (yes)
    : TE-CFP, TE-CFP::Rejected;
elseif (CFP is Waitlisted) then (yes)
    : TE-CFP, TE-CFP::Waitlist;
else  (nothing)
    : TE-CFP, TE-CFP::missed;
endif
stop

```


#### Content Creation 

This can be any content request, Webcast, Interview, Meetup, etc. These content issues first get the parent `tech-evangelism` label applied, and are then transitioned through different stages using the scoped labels below:

| **CFP Labels** |  **Description** |
| ------ | ------ |
| `TE-Content::new-request` |  New content request | 
| `TE-Content::draft` |  A member of the TE team is working on the content |
| `TE-Content::review` |  Content Author is seeking review from other team memebers| 
| `TE-Content::published` |   Content has been published |
| `TE-Content::Repurposing` |  Content is being repurposed on other platforms or mediums | 
| `TE-Content::cancelled` | Content is no longer needed and no one is working on it | 

```plantuml
start
: tech-evangelism, TE-Content::new-request;
if (Content Request Cancelled at any stage) then (yes)
     :tech-evangelism, TE-Content::cancelled;
else
    : tech-evangelism, TE-Content::draft;
    : tech-evangelism, TE-Content::review;
    : tech-evangelism, TE-Content::published;
    : tech-evangelism, TE-Content::Repurposing;
endif

stop
```

### Slack
GitLab team members can also reach us at any time on the [#tech-evangelism](https://app.slack.com/client/T02592416/CMELFQS4B) Slack channel where we share updates, ideas, and thoughts with each other and the wider team.


## How we manage CFPs (Call for Proposals)

### Our events list
Every year, technical evangelism prioritizes some key events in our ecosystem for which, we run the conference proposal (CFP) process. These events are mainatined in a [living list](https://docs.google.com/spreadsheets/u/1/d/1E4J4Kx7Eq8JXuh_o4RfSRbGyjj8IeVCiPRLOGslLcfU/edit?usp=drive_web&ouid=101407496565734994973) as we add suggestions that fulfill our requirements for focus events. You can also find a calendar view of our events below:

<br>

<iframe src="https://calendar.google.com/calendar/embed?src=gitlab.com_5nsh2o42detjnpana2fheauirg%40group.calendar.google.com&ctz=Europe%2FAmsterdam" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

### CFP Management
For every CFP process we are participating in, a [CFP meta issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFP-Meta) is created with details about the event, requirements for CFP submissions and any other relevant information that might be useful to potential speakers. Every person who wants to submit a proposal for an event we are tracking should use the [CFP-Submission](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFPsubmission) issue template in [Corporate Marketing issue tracker](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues) and mark it as related to the Event's CFP Meta issue. This way we are able to track which submissions were made for which event.



## Useful links
1. [How to be an evangelist](/handbook/marketing/technical-evangelism/how-to-be-an-evangelist/)
2. [How to submit a successful conference proposal](/handbook/marketing/technical-evangelism/writing-cfps/)
3. [Consortiums we work with](/handbook/marketing/technical-evangelism/consortiums/)
4. [Speaking logistics](/handbook/marketing/technical-evangelism/speaking-logistics/)
