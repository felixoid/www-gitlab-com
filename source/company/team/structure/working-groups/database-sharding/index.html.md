---
layout: markdown_page
title: "Database Sharding Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | February 11, 2020 |
| Target End Date | August 11, 2020|
| Slack           | [#wg_database-sharding](https://gitlab.slack.com/archives/CTNSZFHEZ) (only accessible from within the company) |
| Google Doc      | [Database Sharding Working Group Agenda](https://docs.google.com/document/d/1_sI-P2cLYPHlzDiJezI0YZHjWAC4BSKJ8aL0cNduDlo/edit#) (only accessible from within the company) |
| Issue Board     | TBD             |

## Business Goal

Database sharding and partitioning will improve availability, scalability and performance.  Sharding will also allow us to enable a path forward for data isolation.  As we continue to investigate implementations and technologies, we will test our hypotheses and add more detail about improvements in the areas below, listed in priority order.  

1. Availability - the database will no longer be a single point of failure as it is today.  Sharding will allow us to spread data across multiple servers and better isolate database outages
1. Scalability - sharding will allow us to horizontally scale at the database tier
1. Performance - partitioning will provide performance enhancements in several identified areas such as search and audit log tables

## Development Plan

The rollout of PostgreSQL 11 to GitLab.com is being done in parallel with the research and development for partitioning and subsequently sharding.  Our approach is to first deliver an MVC implementation of partitioning and use the knowledge and lessons learned to better inform our sharding approach.  In PostgreSQL, sharding is built on top of partitioning.  By starting with partitioning first we can remove the infrastructure concerns and focus on the implentation details and potential complications that we may encounter.  Once we have a working partitioning implementation we can advance to a sharding solution.  

### Development Plan Caveats

- Data Isolation - our initial investigations into how to partition data are focused on [Range, List and Hash Partitioning](https://www.postgresql.org/docs/11/ddl-partitioning.html#DDL-PARTITIONING-OVERVIEW).  We are identifying a [tenancy model](https://gitlab.com/gitlab-org/gitlab/-/issues/196224) in support of the partitioning models listed above, however we are not currently exploring data isolation per tenant.
- CockroachDB - CockroachDB has been mentioned as a possible technology solution for our eventual sharding implementation.  It is not currently considered a viable option for our needs at this point in time.  There are concerns about scale and support for features we use in PostgreSQL.  More details and comments in this issue: [Support CockroachDB as a backing store](https://gitlab.com/gitlab-org/gitlab/-/issues/24143)

## Exit Criteria

- Infra: [PostgreSQL 11 deployed on GitLab.com](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/106) - April
  - Distribution: [Add support for PostgreSQL 11](https://gitlab.com/groups/gitlab-org/-/epics/2414) (13.0)
- Deploy MVC partition (Dependent on PG11 Deployment)
  - Define partition key (may be different than [tenancy model](https://gitlab.com/gitlab-org/gitlab/-/issues/196224) for MVC)
  - Identify MVC [candidate](https://gitlab.com/gitlab-org/gitlab/-/issues/201871) for partitioning implementation
  - Implement partitioning MVC
  - Document process to enable backend teams to implement their own partitioning solution going forward
  - Measure results
- Implement sharding strategy (Informed by partitioning implementation)
  - [Explore CitusDB as a sharding solution](https://gitlab.com/gitlab-org/gitlab/issues/207833)
  - Identify shard key (e.g. [Range, List, Hash](https://www.postgresql.org/docs/12/ddl-partitioning.html#DDL-PARTITIONING-OVERVIEW))
  - Implement and Demonstrate POCs
  - Gather feedback and metrics from POCs
  - Roll out sharding implementation

## Specific Lines of Enquiry

- [Upgrade to PostgreSQL 11 timeline](https://gitlab.com/groups/gitlab-org/-/epics/2184)
- [Infrastructure - Upgrade to PostgreSQL 11](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/6505)
- Testing PostgreSQL 11 upgrade
- [Database Paritioning](https://gitlab.com/groups/gitlab-org/-/epics/2023)
- [Database Sharding](https://gitlab.com/groups/gitlab-org/-/epics/1854)

## Roles and Responsibilities

| Working Group Role                       | Person                          | Title                                    |
|------------------------------------------|---------------------------------|------------------------------------------|
| Executive Stakeholder                    | Christopher Lefelhocz           | Senior Director of Development           |
| Facilitator                              | Craig Gomes                     | Engineering Manager, Database            |
| DRI for Database Sharding                | Craig Gomes                     | Engineering Manager, Database            |
| Functional Lead                          | Nailia Iskhakova                | Software Engineer in Test, Database      |
| Functional Lead                          | Josh Lambert                    | Senior Product Manager, Geo              |
| Functional Lead                          | Gerardo "Gerir" Lopez-Fernandez | Engineering Fellow, Infrastructure       |
| Functional Lead                          | Stan Hu                         | Engineering Fellow, Development          |
| Functional Lead                          | Andreas Brandl                  | Staff Backend Engineer, Database         |
| Member                                   | Chun Du                         | Director of Engineering, Enablement      |
| Member                                   | Pat Bair                        | Senior Backend Engineer, Database        |
| Member                                   | Tanya Pazitny                   | Quality Engineering Manager, Enablement  |
| Member                                   | Mek Stittri                     | Director of Quality Engineering          |

## Meeting Recap

The agenda doc can be found in our Google Drive when searching for "Database Sharding Working Group Agenda"

- 2020-02-10 - First meeting.  Validate attendees, priorities and timing of meetings
  - Blocker identified - We can't ship any changes until PG11 is shipped
  - Blocker identified - We can't use declarativie partioning until completion of [Use structure.sql instead of schema.rb](https://gitlab.com/gitlab-org/gitlab/-/issues/29465)
  - Limiting factor - Database team only consists of Andreas plus Pat who joined 2 weeks prior
- 2020-02-19
  - PG11 testing on reference architecture initiated
  - Capacity planning started to determine head room of current architecture
  - Clarity on expectations discussed - performance and scalability
  - Issue created to investigate issues where sharding would have helped
- 2020-02-24
  - Partitioning Issue [Spike](https://gitlab.com/gitlab-org/gitlab/issues/201871)
  - Handbook entry published [On table partitioning](https://about.gitlab.com/handbook/engineering/development/enablement/database/doc/partitioning.html)
  - PG11 Nightly pipeline for [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab/issues/200036#note_291786476) 
  - PG11 10k Reference Architecture [test](https://gitlab.com/gitlab-org/quality/team-tasks/issues/389#note_292242462)
- 2020-03-02
  - Exploration of CitusDB [started](https://gitlab.com/gitlab-org/gitlab/issues/207833)
- 2020-03-09
  - Incident Review [feedback ](https://gitlab.com/gitlab-org/gitlab/issues/207327#breakdown-by-priority-and-severity)
  - Handbook entry update to explicitly set [priorities](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/42496/diffs) for sharding
  - Handbook entry [Introducing PostgreSQL table partitioning to GitLab's database](https://about.gitlab.com/handbook/engineering/development/enablement/database/doc/partitioning.html)
- 2020-03-16
  - Partitioning spike [complete](https://gitlab.com/gitlab-org/gitlab/-/issues/201871)
  - Run GDK against a [Citus Installation](https://gitlab.com/gitlab-org/gitlab/-/issues/207833#note_302891589)
- 2020-03-23
  - [Identify MVC Tenancy Model for Partitioning](https://gitlab.com/gitlab-org/gitlab/-/issues/196224#note_307785195)
  - Unblocked from using PG11 Features when it is available with completion of [Use structure.sql instead of schema.rb](https://gitlab.com/gitlab-org/gitlab/-/issues/29465)
  - [Partitioning: Investigate increase in planning time (with PG11 and PG12)](https://gitlab.com/gitlab-org/gitlab/-/issues/209800)
  - Yannis Roussos joins Database team
- 2020-03-30
  - Set up Citus [Cluster](https://gitlab.com/gitlab-org/gitlab/-/issues/210554) for experimentation
- 2020-04-06
  - Discussions with Legal regarding Citus Community AGPL licensing
  - Discussions with Citus to discuss Enterprise Licensing model
  - Published results for [Partitioning: Implement cascading deletes without foreign keys](https://gitlab.com/gitlab-org/gitlab/-/issues/201872#note_317474157)
  - Blocker Identified - Test data for Citus Cluster is a concern
- 2020-04-13
  - Follow up meeting with Citus to discuss licensing model and pricing negotiations
  - Data collection on table sizes to better understand impact of performance and migrations
- 2020-04-20
  - Determined that Citus is a non-starter for community edition due to AGPL and Enterprise due to licensing costs
  - Service Extraction discussions
- 2020-04-27
  - Sharding Brainstorming discussions continue
  - [Partitioning/sharding and isolating access patterns](https://gitlab.com/gitlab-org/database-team/team-tasks/-/issues/50)
- 2020-05-04
  - [Postgres sharding with partitioning and FDW](https://gitlab.com/gitlab-org/database-team/team-tasks/-/issues/53)
  - [Part 1 - partitioning + FDW](https://www.youtube.com/watch?v=MiZFtM84x44)
  - [Part 2 - Schema migrations](https://www.youtube.com/watch?v=nt4Khi9Gr3o)
  - [Part 3 - Reference tables with logical replication](https://www.youtube.com/watch?v=ztQtNmSYmEo)
  - MR [WIP / experimental: Postgres partitions + FDW](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/30715)
- 2020-05-11
  - PG11 Shipped to production - we are now unblocked from being able to push MRs to production
  - Focus on [Audit Events](https://gitlab.com/groups/gitlab-org/-/epics/3206) for partitioning
  - [Partitioning: Identify access patterns for Audit Events](https://gitlab.com/gitlab-org/gitlab/-/issues/216653)
- 2020-05-18
  - [Capacity assessment for our main production DB cluster](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10258)
  - Created issue to quantify [Conflicting features for namespace sharding](https://gitlab.com/gitlab-org/database-team/team-tasks/-/issues/50)
  - Work underway for Audit Events partitioning
    - [Migration strategy to partition existing tables: Create partitioned tables](https://gitlab.com/gitlab-org/gitlab/-/issues/202618)
    - [Partitioning: Identify access patterns for Audit Events](https://gitlab.com/gitlab-org/gitlab/-/issues/216653)
- 2020-05-26
  - [Audit log database design changes](https://gitlab.com/gitlab-org/gitlab/-/issues/217471)
  - [Create script to generate test audit_events data](https://gitlab.com/gitlab-org/gitlab/-/issues/219055)
  - [Compatible and conflicting features for namespace sharding](https://gitlab.com/gitlab-org/database-team/team-tasks/-/issues/50)
- 2020-06-01
  - There were a few topics of discussion for this working group
    - `Tenant Sharding` vs. `Namespace Sharding`
    - Goals - Availability, Scalability, Customer Isolation
  - We spent some time discussing different implementation approaches
  - [Database Capacity and Saturation Analysis (Iteration 1)](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10340)
    - Snippet: Given 3-month timeframe we have analyzed, we have a high-degree of confidence that the current architecture is in good shape to handle our needs over the next 12 months
  - Progress continues on [Partitioning: Design and implement partitioning strategy for Audit Events](https://gitlab.com/groups/gitlab-org/-/epics/3206)
  